# Classes, datatypes (KEYWORD1)
Button_t	KEYWORD1

# Methods and Functions (KEYWORD2)
Enable	KEYWORD2
Value_Sp	KEYWORD2
Value_Op	KEYWORD2
Value_Op_Changed	KEYWORD2
Configure	KEYWORD2
Attach	KEYWORD2
Init	KEYWORD2
Deinit	KEYWORD2
Get	KEYWORD2

# Constants (LITERAL1)
TRUE	LITERAL1
FALSE	LITERAL1