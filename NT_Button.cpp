/*
 * @file NT_Button.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Button function definitions.
 */

#include "Arduino.h"
#include "NT_Button.h"

Button_t::Button_t()
{
}

void Button_t::Configure(unsigned long Debounce_Delay)
{
	_Debounce_Delay = Debounce_Delay;
}

void Button_t::Attach(int Pin, bool PullUp_Enable)
{
	_Pin = Pin;

	if (PullUp_Enable == TRUE)
	{
		pinMode(_Pin, INPUT_PULLUP);
		_Value_Sp_Last = TRUE;
	}
	else
	{
		pinMode(_Pin, INPUT);
		_Value_Sp_Last = FALSE;
	}
}

void Button_t::Init()
{
	Enable = TRUE;
}

void Button_t::Deinit()
{
	Enable = FALSE;
}

bool Button_t::Get()
{
	if (Enable == TRUE)
	{
		int Value_Sp = digitalRead(_Pin);

		if (Value_Sp != _Value_Sp_Last)
		{
			if ((millis() - _Last_Debounce_Time) >= _Debounce_Delay)
			{
				Value_Op = Value_Sp;

				Value_Op_Changed = TRUE;

				_Last_Debounce_Time = millis();
			}
		}
		else
		{
			Value_Op_Changed = FALSE;
		}

		_Value_Sp_Last = Value_Sp;
	}
	else
	{
		// Do nothing
	}

	return Value_Op;
}