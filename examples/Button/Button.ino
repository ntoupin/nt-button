/*
   @file Button.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Simple example of button utilisation.
*/

#include <NT_Button.h>

Button_t Button_1;

bool Led_State = FALSE;

void setup()
{
  Button_1.Configure(500);
  Button_1.Attach(6, TRUE);
  Button_1.Init();

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, Led_State);
}

void loop()
{
  Button_1.Get();

  if (Button_1.Value_Op_Changed == TRUE)
  {
    if (Led_State == TRUE)
    {
      digitalWrite(LED_BUILTIN, LOW);
      Led_State = FALSE;
    }
    else
    {
      digitalWrite(LED_BUILTIN, HIGH);
      Led_State = TRUE;
    }
  }
}
