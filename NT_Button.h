/*
 * @file NT_Button.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Button function declarations.
 */

#ifndef NT_Button_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Button_h

class Button_t
{
public:
	Button_t();
	bool Enable = FALSE;
	bool Value_Sp = FALSE;	
	bool Value_Op = FALSE;
	bool Value_Op_Changed = FALSE;
	void Configure(unsigned long Debounce_Delay);
	void Attach(int Pin, bool PullUp_Enable);
	void Init();
	void Deinit();
	bool Get();

private:
	int _Pin;
	bool _Value_Sp_Last = TRUE;
	unsigned long _Debounce_Delay = 0;
	unsigned long _Last_Debounce_Time = 0;
};

#endif